import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {ItemModel} from "../../models/item/item.model";
import {ShoppingListServices} from "../../services/shopping-list/shopping-list.services";
import {ToastServices} from "../../services/shopping-list/toast/toast.services";

/**
 * Generated class for the AddShoppingItemPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-shopping-item',
  templateUrl: 'add-shopping-item.html',
})
export class AddShoppingItemPage {
  item: ItemModel = {
    name: '',
    quantity: undefined,
    price: undefined
  };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private shopping: ShoppingListServices,
    private toast: ToastServices) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddShoppingItemPage');
  }

  addItem(item: ItemModel) {
    this.shopping.addItem(item).then(ref => {
      console.log(ref.key);
      this.toast.show('${item.name} saved!');
      this.navCtrl.setRoot('HomePage', {key: ref.key});
    })
  }

}
