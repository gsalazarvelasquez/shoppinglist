import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {ItemModel} from "../../models/item/item.model";
import {ShoppingListServices} from "../../services/shopping-list/shopping-list.services";
import {ToastServices} from "../../services/shopping-list/toast/toast.services";

/**
 * Generated class for the EditShoppingItemPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-edit-shopping-item',
  templateUrl: 'edit-shopping-item.html',
})
export class EditShoppingItemPage {

  item: ItemModel;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private shopping: ShoppingListServices,
              private  toast: ToastServices) {
  }

  ionViewWillLoad() {
    console.log(this.navParams.get('item'));
    this.item = this.navParams.get('item');
  }

  saveItem(item: ItemModel) {
    this.shopping.editItem(this.item)
      .then(() => {
        this.toast.show('${item.name} saved!');
        this.navCtrl.setRoot('HomePage');
      })
  }

  removeItem(item: ItemModel) {
    this.shopping.removeItem(item)
      .then(() => {
        this.toast.show('${item.name} deleted!');
        this.navCtrl.setRoot('HomePage');
      })
  }
}
