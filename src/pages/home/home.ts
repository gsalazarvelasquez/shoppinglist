import {Component} from '@angular/core';
import {IonicPage, NavController} from 'ionic-angular';
import {ShoppingListServices} from "../../services/shopping-list/shopping-list.services";
import {Observable} from "rxjs/Observable";
import {ItemModel} from "../../models/item/item.model";
import {AddShoppingItemPage} from "../add-shopping-item/add-shopping-item";


@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  addShoppingPage = AddShoppingItemPage

  shoppingList$: Observable<ItemModel[]>;

  constructor(public navCtrl: NavController, private shopping: ShoppingListServices) {
    this.shoppingList$ = this.shopping
      .getShoppingList() // DB LIST
      .snapshotChanges() // Key and value
      .map(changes => {
        return changes.map(c => ({
          key: c.payload.key, ...c.payload.val()
        }));
      });
  }
// {
//   key: 'value-here',
//   name: 'iPad Pro'
// }
}
