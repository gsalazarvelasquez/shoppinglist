import {AngularFireDatabase} from "angularfire2/database";
import {ItemModel} from "../../models/item/item.model";
import {Injectable} from "@angular/core";

@Injectable()
export class ShoppingListServices {

  private shoppingListReference = this.db.list<ItemModel>('shopping-list');

  constructor(private db: AngularFireDatabase) {

  }

  getShoppingList() {
    return this.shoppingListReference;
  }

  addItem(item: ItemModel) {
    return this.shoppingListReference.push(item);
  }

  editItem(item: ItemModel) {
    return this.shoppingListReference.update(item.key, item);
  }

  removeItem(item: ItemModel) {
    return this.shoppingListReference.remove(item.key);
  }
}
