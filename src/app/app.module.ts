import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import {SplashScreen} from '@ionic-native/splash-screen';
import {StatusBar} from '@ionic-native/status-bar';
import {AngularFireModule} from "angularfire2";
import {FIREBASE_CONFIG} from "./firebase.credencial";
import {AngularFireDatabaseModule} from "angularfire2/database";


import {MyApp} from './app.component';
import {ShoppingListServices} from "../services/shopping-list/shopping-list.services";
import {ToastServices} from "../services/shopping-list/toast/toast.services";

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(FIREBASE_CONFIG),
    AngularFireDatabaseModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ShoppingListServices,
    ToastServices
  ]
})
export class AppModule {
}
