export interface ItemModel {
  key?: string;
  name: string;
  quantity: number;
  price: number;
}
